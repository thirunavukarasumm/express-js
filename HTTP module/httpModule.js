const http = require("http");
const PORT = 5000;
const server = http.createServer((req, res) => {
    if (req.url === "/") {
        res.end("Home Page");
    } else if (req.url === "/about") {
        res.end("About page");
    } else {
        res.end(`
            <h1>Oops</h1>
            <p> We can't find the page</p>
        `)
    }
}).listen(PORT,()=>{
    console.log(`Server is listening in the port - ${PORT}`);
});