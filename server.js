const express = require("express");
const app = express();
const userRouter = require("./routes/users")
app.set("view engine","ejs");
// app.use(logger); // This should be at the top if all the routes want to use it.
app.get("/", logger,logger,(req, res) => {
    // Sends Hi to the route.
    // res.send("Hi!");

    // Sends status.
    // res.sendStatus(500);

    // Handles an error.
    // res.status(500).send("Server error oh ooo!");

    // Sends a json.
    // res.status(500).json({message:"Error"});

    // Download a file.
    // res.download("server.js"); // When reloaded the server.js file will be downloaded.
    res.render("index",{text1:"World..."});
}).listen(5000);



app.use("/users",userRouter)

function logger(req,res,next){
    console.log(req.originalUrl);
    next();
}