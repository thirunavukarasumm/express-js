const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
    res.send("Users")
})

router.get("/new", (req, res) => {
    res.send("Users new")
})


// router.get("/:id",(req,res)=>{
//     res.send(`User ID - ${req.params.id}`)
// })
// router.put("/:id", (req, res) => {
//     res.send(`User ID - ${req.params.id}`)
// })
// router.post("/:id", (req, res) => {
//     res.send(`User ID - ${req.params.id}`)
// })
// router.delete("/:id", (req, res) => {
//     res.send(`User ID - ${req.params.id}`)
// })

// This all can be done using the route() method and the rest can be chained.

router 
.route("/:id")
    .get((req, res) => {
        res.send(`User ID - ${JSON.stringify(req.user)}`)
    })
    .post((req, res) => {
        res.send(`User ID - ${req.params.id}`)
    })
    .put((req, res) => {
        res.send(`User ID - ${req.params.id}`)
    })
    .delete((req, res) => {
        res.send(`User ID - ${req.params.id}`)
    });

let users = [{name:"Thiru"},{name:"thiru"}];
router.param("id",(req,res,next,id)=>{
    req.user = users[id];
    next();
})

module.exports = router;